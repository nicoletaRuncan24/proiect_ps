package proiect_ps.demo.Facade;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import proiect_ps.demo.DAO.HolidayRepository;
import proiect_ps.demo.Model.Holiday;
import proiect_ps.demo.Model.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Aceasta clasa are ca si atribut o instanta a clasei  HolidayRepository, si contine logica de baza pe tabela holiday din baza de date.
 */
@Service
public class HolidayFacade {
    @Autowired
    private final HolidayRepository holidayRepository;

    public HolidayFacade(HolidayRepository holidayRepository) {
        this.holidayRepository = holidayRepository;
    }

    /**
     * Aceasta metoda apeleaza metoda findAll pentru a cauta in baza de date toate ofertele de vacanta din tabela holiday.
     * @return lista tuturor ofertelor de vacanta care se gasesc in tabela holiday.
     */
    public List<Holiday> getHolidays() {

        return (List<Holiday>)holidayRepository.findAll();
    }

    /**
     * Aceasta metoda apeleaza metoda findById pentru a cauta in baza de date, oferta de acanta care are id-ul dat ca parametru.
     * @param id
     * @return oferta de vacanta in functie de id-ul dat ca si parametru
     */
    public Holiday getHolidayByID(Long id) {

        return holidayRepository.findById(id).get();

    }

    /**
     * Aceasta metoda apeleaza save pentru a inregistra o noua oferta de vacanta in tabela holiday.
     * @param newHoliday
     * @return oferta de vacanta nou adaugat in tabela holiday
     */
    public Holiday addNewHoliday(Holiday newHoliday) {

        return holidayRepository.save(newHoliday);
    }

    /**
     * @param newHoliday
     * @param id
     * @return oferta de vacanta cu id-ul dat ca parametru actualizata cu datele obiectului Holiday dat tot ca si paraemtru
     */
    public Holiday updateHoliday(Holiday newHoliday,  Long id)
    {
        return holidayRepository.findById(id)
                .map(holiday -> {
                    holiday.setPrice(newHoliday.getPrice());
                    holiday.setStartDate(newHoliday.getStartDate());
                    holiday.setEndDate(newHoliday.getEndDate());
                    return holidayRepository.save(holiday);
                })
                .orElseGet(() -> {
                    newHoliday.setId(id);
                    return holidayRepository.save(newHoliday);
                });
    }

    /**
     * Aceasta metoda apeleaza metoda deleteById care sterge o inregistrare din tabela holiday care are id-ul dat ca si paraemtru.
     * @param id
     */
    public void deleteHoliday(Long id)
    {
        holidayRepository.deleteById(id);
    }

    /**
     * Aceasta metoda apeleaza metoda findByLocation si cauta in baza de date toate ofertele de vacanta din aceeasi locatie.
     * @param location
     * @return lista tuturor ofertelor de vacanta care are aceeasi locatie data ca si paraemtru metodei.
     */
    public List<Holiday> getAllHolidays(String location)
    {
        return holidayRepository.findByLocation(location);
    }

    public Long getIdLocation(String location,String startDate, String endDate)
    {
        List<Holiday> holidays=new ArrayList<Holiday>();
        holidays=this.getHolidays();
        Holiday holiday=null;
        LocalDate sDate = LocalDate.parse(startDate);
        LocalDate eDate = LocalDate.parse(endDate);
        for(Holiday u:holidays)
        {
            if(location.equals(u.getLocation()) && sDate.equals(u.getStartDate()) && eDate.equals(u.getEndDate()))
            {
                holiday=u;
                break;
            }
        }
        return holiday.getId();
    }
}
