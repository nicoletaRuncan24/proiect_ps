package proiect_ps.demo.Facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import proiect_ps.demo.DAO.ReservationsRepository;
import proiect_ps.demo.Model.Holiday;
import proiect_ps.demo.Model.Reservations;
import proiect_ps.demo.Model.User;


import java.util.List;

/**
 * Aceasta clasa are ca si atribut o instanta a clasei ReservationsRepository, si contine logica de baza pe tabela reservations din baza de date.
 */
@Service
public class ReservationsFacade {

    @Autowired
    private final ReservationsRepository reservationsRepository;

    public ReservationsFacade(ReservationsRepository reservationsRepository) {
        this.reservationsRepository = reservationsRepository;
    }

    /**
     * @return lista tutoror rezervarilor inregistrate in baza de date.
     */
    public List<Reservations> getAllReservations()
    {
        return (List<Reservations>)reservationsRepository.findAll();
    }


    /**
     * @param reservation
     * @return rezervarea proaspat adaugata in baza de date
     */
    public Reservations addReservation(Reservations reservation)
    {

        return reservationsRepository.save(reservation);
    }

    /**
     * @param id
     * @return lista rezervarilor facute de un utilizator in functie de id-ul sau.
     */
    public List<Reservations> getReservationsByUserId(Long id)
    {
        return reservationsRepository.findByUserID(id);
    }
}
