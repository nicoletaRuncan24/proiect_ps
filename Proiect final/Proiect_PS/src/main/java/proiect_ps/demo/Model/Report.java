package proiect_ps.demo.Model;

import java.util.List;

/**
 * Aceasta clasa abstracta reprezinta definirea unui Raport generic, care contine ca si atribute o lista de utilizatori,
 * o lista de vacante si o lista de rezervari. De asemenea contine si metoda abstracta getReport(), care va fi implementata
 * in fiecare din cele doua clase mostenitoare ale acestei clase
 */
public abstract class Report {
    private List<User> users;
    private List<Reservations> reservations;
    private List<Holiday> holidays;
    private ReportType reportType;

    public Report(List<User> users, List<Reservations> reservations, List<Holiday> holidays, ReportType reportType) {
        this.users = users;
        this.reservations = reservations;
        this.holidays = holidays;
        this.reportType = reportType;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Reservations> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservations> reservations) {
        this.reservations = reservations;
    }

    public List<Holiday> getHolidays() {
        return holidays;
    }

    public void setHolidays(List<Holiday> holidays) {
        this.holidays = holidays;
    }

    public ReportType getReportFactory() {
        return reportType;
    }

    public void setReportType(ReportType reportType) {
        this.reportType = reportType;
    }

    public abstract String getReport();
}
