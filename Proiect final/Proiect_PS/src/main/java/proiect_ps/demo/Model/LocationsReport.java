package proiect_ps.demo.Model;

import java.util.List;

/**
 * Aceasta clasa defineste un tip de raport, si mai exact un raport cu toate ofertele de vacanta disponibile dintr-o
 * anumita locatie.
 */
public class LocationsReport extends Report{

    public LocationsReport(List<User> users, List<Reservations> reservations, List<Holiday> holidays) {
        super(users, reservations, holidays, ReportType.LOCATION_REPORT);
    }

    /**
     * Aceasta metoda implementeaza metoda abstracta din clasa abstracta Report si construieste mesajul care ii este transmis
     * utilizatorului despre toate ofertele de vacanta dinsponibile dintr-o anumita locatie.
     * @return raportul compus din toate ofertele disponibile de vacanta dintr-o anumita locatie.
     */
    @Override
    public String getReport() {
        String print_string="Ofertele de vacanta disponibile pentru locatia cautata sunt:"+'\n';

        for(Holiday h:this.getHolidays())
        {
            print_string=print_string+h.getLocation()+": Din data de: " + h.getStartDate()+ " pana in data de: "+h.getEndDate()+" la pretul de: "+h.getPrice()+'\n';
        }

        return print_string;
    }
}
