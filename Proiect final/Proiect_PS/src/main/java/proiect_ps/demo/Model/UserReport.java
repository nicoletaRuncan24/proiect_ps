package proiect_ps.demo.Model;

import java.util.List;

/**
 * Aceasta clasa defineste un tip de raport, si mai exact un raport pentru istoricul de calatorii pe care l-a rezervat
 * un utilizator prin intermediul acestei aplicatii.
 */
public class UserReport extends Report{

    public UserReport(List<User> users, List<Reservations> reservations, List<Holiday> holidays) {
        super(users, reservations, holidays, ReportType.USER_REPORT);
    }

    /**
     * Aceasta metoda implementeaza metoda abstracta din clasa abstracta Report si construieste mesajul care ii este transmis
     * utilizatorului despre rezervarile facute in trecut.
     * @return un mesaj prin care ii sunt prezentate utilizatorului, locurile pe care le-a vizitat
     */
    public String getReport()
    {
        String print_string="Istoric: "+'\n'+"Ati calatorit in locatiile cu urmatoarele id-uri: ";

        for(Reservations r:this.getReservations())
        {
            print_string=print_string+" "+r.getIdLocatie()+',';
        }

        return print_string;
    }
}
