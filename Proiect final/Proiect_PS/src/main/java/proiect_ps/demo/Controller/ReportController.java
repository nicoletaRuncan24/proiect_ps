package proiect_ps.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import proiect_ps.demo.Model.Reservations;
import proiect_ps.demo.Service.ReportService;
import proiect_ps.demo.Service.ReservationsService;

import java.util.List;

/**
 * Aceasta clasa participa la  interactiunea cu web services prin metode tip GET, PUT, POST si DELETE pentru transmiterea datelor dintre
 * aplicatie si baza de date, dar si invers
 */
@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/report")
public class ReportController {
    @Autowired
    private ReportService reportServ;

    /**
     * @param location
     * @return mesajul corespunzator raportului de tip LocationsReport
     */
    @GetMapping("/getLocationReport/{location}")
    public String getUserReport(@PathVariable String location)
    {
        return reportServ.getLocationsReport(location);
    }

    /**
     * @param idUser
     * @return mesajul corespunzator raportului de tip UserReport.
     */
    @GetMapping("/getUserReport/{idUser}")
    public String getUserReport(@PathVariable Long idUser)
    {
        return reportServ.getUserReport(idUser);
    }
}
