package proiect_ps.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import proiect_ps.demo.DAO.UserRepository;
import proiect_ps.demo.Model.User;
import proiect_ps.demo.Service.UserService;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

/**
 * Aceasta clasa participa la  interactiunea cu web services prin metode tip GET, PUT, POST si DELETE pentru transmiterea datelor dintre
 * aplicatie si baza de date, dar si invers(tabela users).
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userServ;
    /**
     * Aceasta metoda participa la afisarea tuturor obiectelor de tip User retinute in baza de date a aplicatiei.
     * @return lista tuturor utilizatorilor aplicatiei.
     */
    @GetMapping("/getUser")
    public List<User> all() {

       return userServ.getAllUsers();
    }

    /**
     *Aceasta metoda participa la introducerea in baza de date a unui utilizator proaspat autentificat.
     * @param newUser
     * @return obiectul utilizatorului proaspat autentificat in aplicatie
     */
    @PostMapping(path="/addUser", consumes="application/json", produces="application/json")
    public User newUser(@RequestBody User newUser) {
       return userServ.addNewUser(newUser);
    }

    /**
     * Aceasta metoda participa la identificarea si returnarea unui obiect de tip Utilizator din baza de date cu un anumit id.
     * @param id
     * @return utilizatorul care are id-ul dat ca si parametru functiei.
     */
    @GetMapping("/getUserID/{id}")
    public User getOneUserID(@PathVariable Long id) {
       return userServ.getOneUser(id);
    }

    /**
     * Aceasta metoda returneaza un obiect de tip User, care are un anumit username
     * @param user
     * @return utlizatorul care are username-ul identic ca cel dat ca si parametru functiei
     */
    @GetMapping("/getUserU/{user}")
    public User getOneUser(@PathVariable String user) {
       return userServ.getUser(user);
    }

    @GetMapping("/verifyUser/{user}/{password}")
    public User verifyLogin(@PathVariable String user, @PathVariable String password)
    {
        return userServ.verifyLogIn(user, password);
    }

}
