package proiect_ps.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import proiect_ps.demo.Model.Holiday;
import proiect_ps.demo.Model.Reservations;
import proiect_ps.demo.Service.ReservationsService;
import proiect_ps.demo.Service.UserService;

import java.util.List;

/**
 * Aceasta clasa participa la  interactiunea cu web services prin metode tip GET, PUT, POST si DELETE pentru transmiterea datelor dintre
 * aplicatie si baza de date, dar si invers(tabela reservations).
 */
@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/reservation")
public class ReservationsController {

    @Autowired
    private ReservationsService reservationsServ;

    /**
     * @return toate rezerarile inregistrare.
     */
    @GetMapping("/getReservations")
    public List<Reservations> allR() {
        return reservationsServ.getReservations();
    }

    /**
     * @param newReservation
     * @return noua rezervare inregistrata
     */
    @PostMapping(path="/addReservation", consumes="application/json", produces="application/json")
    public Reservations newReservation(@RequestBody Reservations newReservation) {

        return reservationsServ.addReservation(newReservation);
    }

    /**
     * @param idUser
     * @return lista rezervarilor facute de un anumit utilizator cu id-ul idUser
     */
    @GetMapping("/getReservationsForUser/{idUser}")
    public List<Reservations> reservationsForUser(@PathVariable Long idUser) {
        return reservationsServ.getReservationsByUserId(idUser);
    }
}
