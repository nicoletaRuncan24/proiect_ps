package proiect_ps.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import proiect_ps.demo.Facade.ReservationsFacade;
import proiect_ps.demo.Facade.UserFacade;
import proiect_ps.demo.Model.Reservations;
import proiect_ps.demo.Model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Aceasta clasa cuprinde metodele de logica pentru tabela de reservations, pentru a accesa fie toate inregistrarile din tabela, fie doar anumite inregistrari
 * in functie de campul id, sau pentru a inregistra o noua rezervare in tabela.
 */
@Service
public class ReservationsService {

    @Autowired
    private final ReservationsFacade reservationsFacade;

    public ReservationsService(ReservationsFacade reservationsFacade) {
        this.reservationsFacade = reservationsFacade;
    }

    /**
     * @return toate rezervarile existente
     */
    public List<Reservations> getReservations()
    {
        return reservationsFacade.getAllReservations();
    }

    /**
     * @param reservation
     * @return obiectul de tip Reservations proaaspat adaugat in baza de date
     */
    public Reservations addReservation(Reservations reservation)
    {

        return reservationsFacade.addReservation(reservation);
    }

    /**
     * @param id
     * @return toate rezervarile pe care le are un anumit utilizatorul cu id-ul id, dat ca si parametru metodei.
     */
    public List<Reservations> getReservationsByUserId(Long id)
    {

        return reservationsFacade.getReservationsByUserId(id);
    }


}
