package proiect_ps.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import proiect_ps.demo.DAO.HolidayRepository;
import proiect_ps.demo.Facade.HolidayFacade;
import proiect_ps.demo.Facade.UserFacade;
import proiect_ps.demo.Model.Holiday;
import proiect_ps.demo.Model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Aceasta clasa cuprinde metodele de logica pentru tabela de holiday, pentru a accesa fie toate inregistrarile din tabela, fie doar un
 * in functie de campul id, sau pentru a inregistra o noua locatie in tabela, pentru a actualiza o anumita inregistrare din tabela,
 * sau pentru a sterge o inregistrare in functie de id si asa mai departe.
 */
@Service
public class HolidayService extends Observable{

    @Autowired
    private final HolidayFacade holidayFacade;


    public HolidayService( HolidayFacade holidayFacade, UserFacade userFacade) {
        super(userFacade);
        this.holidayFacade = holidayFacade;
    }

    /**
     * Este o functie care returneaza lista tuturor vacantelor disponibile
     * @return lista vacantelor disponibile
     */
    public List<Holiday> getAllH() {
        return holidayFacade.getHolidays();
    }

    /**
     * @param id
     * @return o locatie in functie de un id dat ca si parametru
     */
    public Holiday getHolidayID(Long id) {

        return holidayFacade.getHolidayByID(id);

    }

    /**
     * Aceasta metoda ne adauga o noua oferta de vacanta in baza de date si ne afiseaza oferta adaugata, iar atunci cand este adaugata
     * oferta noua, toti utilizatorii vor fi anuntati prin apelul metodei notifyUsers.
     * @param newHoliday
     * @return obiectul proaspat introdus in baza de date
     */
    public Holiday addHoliday(Holiday newHoliday)
    {
        this.notifyUsers();
        return holidayFacade.addNewHoliday(newHoliday);
    }

    /**
     * Aceasta metoda participa la actualizarea campurilor de pret, data de inceput a vacantei si data de terminare a vacantei.
     * @param newHoliday
     * @param id
     * @return obiectul actualizat sau un nou obiect dat ca si parametru.
     */
    public Holiday replaceHoliday( Holiday newHoliday,  Long id) {

      return holidayFacade.updateHoliday(newHoliday,id);
    }

    /**
     * Aceasta metoda sterge o inregistare din baza de date, in functie de id.
     * @param id
     */
    public void deleteH(Long id)
    {
       holidayFacade.deleteHoliday(id);
    }

    /**
     * Este o metoda care returneaza lista tuturor ofertelor dintr-o anumita locatie.
     * @param location
     * @return lista tuturor ofertelor de vacanta dintr-o anunita locatie.
     */
    public List<Holiday> getOffers(String location)
    {
        return holidayFacade.getAllHolidays(location);
    }

    public Long getIdLocation(String location,String startDate, String endDate)
    {
        return holidayFacade.getIdLocation(location, startDate, endDate);
    }

}
