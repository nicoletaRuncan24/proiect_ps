package proiect_ps.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import proiect_ps.demo.Model.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class FactoryTest {

    @Test
    public void testFactoryPatternType1()
    {
        ReportFactory reportFactory=new ReportFactory();
        Report report=reportFactory.createReport(null, null, null, ReportType.LOCATION_REPORT.toString());
        Assertions.assertTrue(report instanceof LocationsReport);
    }

    @Test
    public void testFactoryPatternType2()
    {
        ReportFactory reportFactory=new ReportFactory();
        Report report=reportFactory.createReport(null, null, null, ReportType.USER_REPORT.toString());
        Assertions.assertTrue(report instanceof UserReport);
    }

    @Test
    public void testMethodGetReport()
    {
        ReportFactory reportFactory=new ReportFactory();
        List<Holiday> holidays=new ArrayList<>();
        Holiday holiday1=new Holiday("Polonia",900, LocalDate.of(2021, 7, 13), LocalDate.of(2021, 7, 20));
        //Holiday holiday2=new Holiday("Polonia",700, LocalDate.of(2021, 6, 10), LocalDate.of(2021, 6, 15));

        holidays.add(holiday1);
        //holidays.add(holiday2);
        Report report=reportFactory.createReport(null, null, holidays, ReportType.LOCATION_REPORT.toString());

        Assertions.assertEquals("Ofertele de vacanta disponibile pentru locatia cautata sunt:"+'\n'+holiday1.getLocation()+": Din data de: " + holiday1.getStartDate()+ " pana in data de: "+holiday1.getEndDate()+" la pretul de: "+holiday1.getPrice()+'\n',report.getReport() );
    }


}
