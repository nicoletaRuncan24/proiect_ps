import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import User from './User'
import Holiday from './Holiday'
import LogIn from './LogIn'
import Final from './Final'

function App() {

  const defaultRoute = window.location.pathname === "/" ? <Redirect to="/user" /> : undefined;

  return (
   <Router>
     <Switch>
       <Route exact path="/User" component={User}/>
     </Switch>
     <Switch>
       <Route exact path="/Holiday" component={Holiday}/>
     </Switch>
     <Switch>
       <Route exact path="/LogIn" component={LogIn}/>
     </Switch>
     <Switch>
       <Route exact path="/Final" component={Final}/>
     </Switch>
     {defaultRoute}
   </Router>
  );
}

export default App;