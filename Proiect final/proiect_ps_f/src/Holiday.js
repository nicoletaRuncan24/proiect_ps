import React from "react";
import {DataGrid} from '@material-ui/data-grid';
import axiosInstance from "axios";
import {Button, Container, TextField, Grid} from  '@material-ui/core';


const columns = [
    {field: "location", headerName: "Location", width: 200 },
    {field: "price", headerName: "Price", width: 200 },
    {field: "startDate", headerName: "Start Date", width: 200 },
    {field: "endDate", headerName: "End Date", width: 200 }
]


export default class Holiday extends React.Component{
    constructor (){
        super();
        this.state={
            holidays : [],
            id : 0,
            idHoliday: 0,
            report:""
        };

        axiosInstance
        .get("http://localhost:8080/holiday/getHoliday")
        .then(response => {
            const val = response.data
            this.setState({holidays: val})
            console.log(val)
        })
        .catch(error =>{
            console.log(error);
        })
    }


    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
        console.log(value);
    };

    handleClick = () => {
        var elem1 = document.getElementById("location")
        var location = elem1.value
        var elem2 = document.getElementById("startDate");
        var startDate=elem2.value
        var elem3=document.getElementById("endDate")
        var endDate=elem3.value
        console.log(location, startDate,endDate)

        
        var idUser = localStorage.getItem("USER_ID")
        console.log(idUser)

        axiosInstance
        .get("http://localhost:8080/holiday/getHolidayID/"+location+"/"+startDate+"/"+endDate)
        .then(response => {
            const val = response.data
            this.setState({idHoliday: val})
            console.log(val)
            
            axiosInstance.post("http://localhost:8080/reservation/addReservation", {
            idUser : idUser,
            idLocatie : val
            })

            axiosInstance.delete("http://localhost:8080/holiday/deleteHoliday/"+val)
             .then(
                res => {
                    const val = res.data;
                    console.log("Succes");
                    this.props.history.push("/final");
                    
                }
            )
        } )
        .catch(error =>{
            console.log(error);
        })

        var values = []
        axiosInstance
        .get("http://localhost:8080/holiday/getHoliday")
        .then( res => {
            values = res.data
            this.setState({holidays: values})
        })
        .catch(error =>{
            console.log(error);
        })

        
        
       
    }
    
    handleClick2 = () => {
        
        var elem = document.getElementById("locatie")
        var locatie = elem.value
       
        axiosInstance.get("http://localhost:8080/report/getLocationReport/"+locatie)
        .then(     res => {
            const val = res.data;
            this.setState({report: val})
            console.log(this.state.report)
            
        })
        .catch(error =>{
            console.log(error);
        })
        
    }

    render (){
        return (
            <div>
               <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginBottom: 50, marginTop:30, color:'magenta', fontSize:30, fontWeight: "bold"}}>
                 Book your vacation with us!
                 </div>
                <div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
                    <div style={{ display: 'flex', height: 600,width:820, background:'gray'}}>
                        <div style={{ flexGrow: 4 }}>
                            <DataGrid rows={this.state.holidays} columns={columns} pageSize={20}/>
                        </div>

                    </div>

                </div>

                <Container maxWidth="sm">
                <div>
                    <Grid>
                        <form onSubmit={this.onSubmitFun}>
                        <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginBottom: 1, marginTop:20, color:'black', fontSize:20, fontWeight:"bold"}}>
                            Introduceti locatia dorita:
                         </div>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="location"
                                label="Location"
                                name="location"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                            />

                         <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginBottom: 1, marginTop:20, color:'black', fontSize:20, fontWeight:"bold"}}>
                            Introduceti data de inceput a vacantei:
                         </div>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="startDate"
                                label="Start Date"
                                name="startDate"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                            />

                       <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginBottom: 1, marginTop:20, color:'black', fontSize:20, fontWeight:"bold"}}>
                            Introduceti data de sfarsit a vacantei:
                         </div>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="endDate"
                                label="End Date"
                                name="endDate"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                            />
                       
                            <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginTop:15, marginBottom:100}}>

                            <Button
                                onClick = { this.handleClick}
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="secondary"
                            
                            >
                            Book
                </Button>
                </div>

                <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginBottom: 1, marginTop:50, color:'black', fontSize:15, fontWeight:"bold"}}>
                            Gaseste ofertele noastre in functie de locatia dorita:
                         </div>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="locatie"
                                label="Locatia dorita"
                                name="locatie"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                            />
                       
                            <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginTop:15, marginBottom:30}}>

                            <Button
                                onClick = { this.handleClick2}
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            
                            >
                            Cauta
                </Button>
                </div>
               
                        </form>
                    </Grid>
                    <div style={{display: 'flex',  justifyContent:'center', alignItems:'center', fontSize:20}}>
                        <div style={{ flexGrow:6, marginLeft:0}}>
                            <p>{this.state.report}</p>
                        </div>
               
                </div>
                </div>
            </Container>
                
            </div>
        );
    }

    
}



