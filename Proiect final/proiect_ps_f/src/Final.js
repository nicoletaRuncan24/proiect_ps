import React from "react";
import {DataGrid} from '@material-ui/data-grid';
import axiosInstance from "axios";
import {Button, Container, TextField, Grid} from  '@material-ui/core';

export default class Final extends React.Component{
    constructor (){
        super();
        this.state={
            report : ""
            
        };
    }


    handleClick = () => {
        
        var idUser = localStorage.getItem("USER_ID")
        console.log(idUser)
       
        axiosInstance.get("http://localhost:8080/report/getUserReport/"+idUser)
        .then(     res => {
            const val = res.data;
            this.setState({report: val})
            console.log(this.state.report)
            
        })
        .catch(error =>{
            console.log(error);
        })
        
    }
        
 render() {
    return (
      
            <div style={{background:'cyan'}}>
            <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginBottom: 50, marginTop:150, color:'primary', fontSize:30, fontWeight: "bold"}}>
             MULTUMIM PENTRU ALEGEREA FACUTA!
             </div>
                <Grid>
                    <form onSubmit={this.onSubmitFun}>
                        
                        <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginTop:250}}>

                        <Button
                            //type="submit"
                            onClick = { this.handleClick}
                            fullWidth
                            variant="contained"
                            color="primary"
                        
                        >
                            Vezi Istoric
            </Button>
            </div>
                    </form>
                    <div style={{display: 'flex',  justifyContent:'center', alignItems:'center', fontSize:20}}>
                        <div style={{ flexGrow:6, marginLeft:100}}>
                            <p>{this.state.report}</p>
                        </div>
               
                </div>
                </Grid>
            </div>
       
    );
 }
}