import React from "react";
import {DataGrid} from '@material-ui/data-grid';
import axiosInstance from "axios";
import InputField       from "./InputField";
import SubmitButton     from "./SubmitButton";
import ReactDataGrid from "react-data-grid";
import {Button, Container, TextField, Grid} from  '@material-ui/core';
import {
    BrowserRouter as Router,
    Link
} from "react-router-dom";


export default class LogIn extends React.Component{
    constructor (){
        super();
        this.state={
            id:0,
            user:"",
            password:"",
            firstName:"",
            lastName:""
        };
        
    }

    handleInput = event => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        });
        console.log(value);
    };

    onSubmitFun = event => {
        event.preventDefault();
        
        axiosInstance.get("http://localhost:8080/user/verifyUser/"+this.state.user+"/"+this.state.password)
            .then(
                res => {
                    const val = res.data;
                    console.log(val);
                    if(val!=="")
                    {
                        localStorage.setItem("USER_ID", res.data.id);
                        //console.log(USER_ID);
                        console.log("Succes");
                        this.props.history.push("/holiday");
                    }
                    
                   
                }
            )
            .catch(error => {
                console.log(error)
            })
    }




    render() {
        return (
            <Container maxWidth="sm">
                <div>
                <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginBottom: 50, marginTop:30, color:'magenta', fontSize:30, fontWeight: "bold"}}>
                 Log In
                 </div>
                    <Grid>
                        <form onSubmit={this.onSubmitFun}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="user"
                                label="Username"
                                name="user"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                onChange={this.handleInput}
                                autoComplete="current-password"
                            />
                            
                            <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginTop:30}}>

                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="secondary"
                            
                            >
                                Sign In
                </Button>
                </div>
                        </form>
                    </Grid>
                </div>
            </Container>
        );
    }


}