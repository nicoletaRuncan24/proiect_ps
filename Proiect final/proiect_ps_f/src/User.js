import React from "react";
import {DataGrid} from '@material-ui/data-grid';
import axiosInstance from "axios";
import InputField       from "./InputField";
import SubmitButton     from "./SubmitButton";
import ReactDataGrid from "react-data-grid";
import {Button, Container, TextField, Grid} from  '@material-ui/core';
import {
    BrowserRouter as Router,
    Link
} from "react-router-dom";


export default class User extends React.Component{
    constructor (){
        super();
        this.state={
            id:0,
            user:"",
            password:"",
            firstName:"",
            lastName:""
        };
        //this.doRegister = this.doRegister.bind(this);
        /*axiosInstance
        .get("http://localhost:8080/user/getUser")
        .then(response => {
            const val = response.data
            this.setState({foods: val})
            console.log("mancare")
            console.log(val)
        })
        .catch(error =>{
            console.log(error);
        })*/
    }


    setInputvalue(property, val){
        if(val.length>40)
        {
            return;
        }
        this.setState({
            [property]:val
        })

    }

    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
        console.log(value);
    };

    onSubmitDelete(){

        let delRes = this.state.id
    
        console.log( delRes)
    
        axiosInstance.delete("/admin/foodItem/" + delRes)
        .then(console.log("ok del"))
        .catch(error =>{
            console.log(error)
        })
    
    }

    onSubmitFun=event=>{
        event.preventDefault();
        if(!this.state.user){
            return;
        }
        if(!this.state.password){
            return;
        }
        if(!this.state.firstName){
            return;
        }
        if(!this.state.lastName){
            return;
        }
        console.log(this.state.user);
        console.log(this.state.password);
        console.log(this.state.firstName);
        console.log(this.state.lastName);

        axiosInstance
        .post("http://localhost:8080/user/addUser", {
            id : null,
            user : this.state.user,
            password : this.state.password,
            firstName : this.state.firstName,
            lastName : this.state.lastName
        })
        .then(
            res => {
                const val = res.data;
                console.log("Succes");
                this.props.history.push("/logIn");
            })
            
          .catch(error=> {
            console.log(error);
          });
    }

    render() {
        return (
            <Container maxWidth="sm">
                <div>
                <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginBottom: 50, marginTop:30, color:'magenta', fontSize:30, fontWeight: "bold"}}>
                 REGISTER
                 </div>
                    <Grid>
                        <form onSubmit={this.onSubmitFun}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="firstName"
                                label="First Name"
                                type="firstName"
                                id="firstName"
                                onChange={this.handleInput}
                                autoFocus
                            />

                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="lastName"
                                label="Last Name"
                                type="lastName"
                                id="lastName"
                                onChange={this.handleInput}
                                autoFocus
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="user"
                                label="Username"
                                name="user"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                onChange={this.handleInput}
                                autoComplete="current-password"
                            />
                            
                            <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginTop:30}}>

                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="secondary"
                            
                            >
                                Sign Up
                </Button>
                </div>
                <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',  marginTop:30}}>
                <Link to="/logIn">
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            
                            >
                            Sign In
                </Button>
                </Link>
                </div>
            </form>
        </Grid>
    </div>
</Container>
        );
    }

}


