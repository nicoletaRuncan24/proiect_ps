package proiect_ps.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import proiect_ps.demo.DAO.UserRepository;
import proiect_ps.demo.Model.User;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserRepository repository;


    @GetMapping("/getUser")
    List<User> all() {
        return (List<User>)repository.findAll();
    }

    @PostMapping(path="/addUser", consumes="application/json", produces="application/json")
    User newUser(@RequestBody User newUser) {
        return repository.save(newUser);
    }

    @GetMapping("/getUser/{id}")
    Optional<User> getOneUser(@PathVariable Long id) {

        return repository.findById(id);

    }



}
