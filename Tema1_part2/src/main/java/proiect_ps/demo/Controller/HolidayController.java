package proiect_ps.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import proiect_ps.demo.DAO.HolidayRepository;
import proiect_ps.demo.Model.Holiday;

import java.util.List;

@RestController
@RequestMapping("/holiday")
public class HolidayController {

    @Autowired
    private HolidayRepository repository;

    @GetMapping("/getHoliday")
    List<Holiday> all() {
        return (List<Holiday>)repository.findAll();
    }

    @PostMapping(path="/addHoliday", consumes="application/json", produces="application/json")
    Holiday newHoliday(@RequestBody Holiday newHoliday) {
        return repository.save(newHoliday);
    }

    @GetMapping("/getHoliday/{location}")
    Holiday getOneUser(@PathVariable String location) {

        return repository.findByLocation(location);

    }

    @PutMapping("/holiday/{id}")
    Holiday replaceHoliday(@RequestBody Holiday newHoliday, @PathVariable Long id) {

        return repository.findById(id)
                .map(holiday -> {
                    holiday.setPrice(newHoliday.getPrice());
                    return repository.save(holiday);
                })
                .orElseGet(() -> {
                    newHoliday.setId(id);
                    return repository.save(newHoliday);
                });
    }

    @DeleteMapping("/deleteHoliday/{id}")
    void deleteHoliday(@PathVariable Long id) {
        repository.deleteById(id);
    }


}
