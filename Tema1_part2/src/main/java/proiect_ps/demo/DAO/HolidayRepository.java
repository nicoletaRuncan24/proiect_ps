package proiect_ps.demo.DAO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import proiect_ps.demo.Model.Holiday;


public interface HolidayRepository extends CrudRepository<Holiday, Long>{
    @Query("SELECT h FROM Holiday h WHERE h.location = ?1")
    public Holiday findByLocation(String Location);
}
