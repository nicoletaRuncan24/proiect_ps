package proiect_ps.demo.DAO;
import org.springframework.data.repository.CrudRepository;
import proiect_ps.demo.Model.User;


public interface UserRepository extends CrudRepository<User, Long> {

}
