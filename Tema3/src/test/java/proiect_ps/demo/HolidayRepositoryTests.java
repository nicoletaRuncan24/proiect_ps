package proiect_ps.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import proiect_ps.demo.DAO.HolidayRepository;
import proiect_ps.demo.DAO.UserRepository;
import proiect_ps.demo.Facade.HolidayFacade;
import proiect_ps.demo.Facade.UserFacade;
import proiect_ps.demo.Model.Holiday;
import proiect_ps.demo.Service.HolidayService;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
@ExtendWith(MockitoExtension.class)
public class HolidayRepositoryTests {

    @Mock
    HolidayRepository holidayRep;

    @Mock
    UserRepository userRep;
    @Test
    public void testAddFunction()
    {
        Holiday holiday=new Holiday("Dubai", 900, LocalDate.of(2021, 8, 9), LocalDate.of(2021, 8, 19));
        HolidayFacade holidayFacade=new HolidayFacade(holidayRep);
        UserFacade userFacade=new UserFacade(userRep);
        HolidayService holidayServ = new HolidayService(holidayFacade, userFacade);
        holidayServ.addHoliday(holiday);
        verify( holidayRep, atLeastOnce()).save(holiday);
    }

    @Test
    public void testReplaceHoliday()
    {
        Holiday holiday=new Holiday("Mexic", 1000, LocalDate.of(2021, 9, 20), LocalDate.of(2021, 9, 25));
        HolidayFacade holidayFacade=new HolidayFacade(holidayRep);
        UserFacade userFacade=new UserFacade(userRep);
        HolidayService holidayServ = new HolidayService(holidayFacade, userFacade);
        holidayServ.replaceHoliday(holiday, Long.valueOf(1));
        verify( holidayRep, atLeastOnce()).save(holiday);
    }

    @Test
    public void testDeleteHoliday()
    {
        HolidayFacade holidayFacade=new HolidayFacade(holidayRep);
        UserFacade userFacade=new UserFacade(userRep);
        HolidayService holidayServ = new HolidayService(holidayFacade, userFacade);
        holidayServ.deleteH(Long.valueOf(1));
        verify( holidayRep, atLeastOnce()).deleteById(Long.valueOf(1));
    }

    @Test
    public void testGetHolidays()
    {
        HolidayFacade holidayFacade=new HolidayFacade(holidayRep);
        UserFacade userFacade=new UserFacade(userRep);
        HolidayService holidayServ = new HolidayService(holidayFacade, userFacade);
        holidayServ.getOffers("Mexic");
        verify( holidayRep, atLeastOnce()).findByLocation("Mexic");
    }

}