package proiect_ps.demo.DAO;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import proiect_ps.demo.Model.Holiday;
import proiect_ps.demo.Model.Reservations;

import java.util.List;

/**
 * Aceasta clasa participa la realizarea conexiunii aplicatiei cu baza de date, mai exact cu tabela reservations.
 */
public interface ReservationsRepository extends CrudRepository<Reservations, Long> {
    @Query("SELECT r FROM Reservations r WHERE r.idUser = ?1")
    public List<Reservations> findByUserID(Long id);

}
