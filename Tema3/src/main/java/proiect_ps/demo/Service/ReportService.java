package proiect_ps.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import proiect_ps.demo.Facade.HolidayFacade;
import proiect_ps.demo.Facade.ReservationsFacade;
import proiect_ps.demo.Facade.UserFacade;
import proiect_ps.demo.Model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * In acesta clasa este creata logica pentru crearea celor doua tipuri de rapoarte.
 */
@Service
public class ReportService {
    @Autowired
    UserFacade userFacade;

    @Autowired
    HolidayFacade holidayFacade;

    @Autowired
    ReservationsFacade reservationsFacade;

    public ReportService(UserFacade userFacade, HolidayFacade holidayFacade, ReservationsFacade reservationsFacade) {
        this.userFacade = userFacade;
        this.holidayFacade = holidayFacade;
        this.reservationsFacade = reservationsFacade;
    }

    /**
     * In aceasta metoda se gaseste logica de creare a unui raport de tipul LocationsReport, fiind apelata si metoda de getReport().
     * @param location
     * @return mesajul obtinut prin apelarea metodei getReport() pentru un obiect de tipul Report creat aici prin metoda createReport.
     */
    public String getLocationsReport(String location)
    {
        List<Holiday> holidaysFromLocation=holidayFacade.getAllHolidays(location);

        ReportFactory reportFactory=new ReportFactory();
        Report report= reportFactory.createReport(null, null, holidaysFromLocation, "LOCATION_REPORT");
        return report.getReport();
    }

    /**
     * n aceasta metoda se gaseste logica de creare a unui raport de tipul UserReport, fiind apelata si metoda de getReport().
     * @param idUser
     * @return mesajul obtinut prin apelarea metodei getReport() pentru un obiect de tipul Report creat aici prin metoda createReport.
     */
    public String getUserReport(Long idUser)
    {
        List<Reservations> reservationForUser=reservationsFacade.getReservationsByUserId(idUser);
        List<Holiday> allHolidays=holidayFacade.getHolidays();
        List<Holiday> holidaysForUser=new ArrayList<>();
        for(Reservations reserv:reservationForUser)
        {
            for(Holiday h:allHolidays)
            {
                if(reserv.getIdLocatie()==h.getId())
                {
                    holidaysForUser.add(h);
                }
            }
        }

        ReportFactory reportFactory=new ReportFactory();
        Report report=reportFactory.createReport(null, reservationForUser, holidaysForUser, "USER_REPORT");
        return report.getReport();
    }
}
