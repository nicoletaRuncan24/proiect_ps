package proiect_ps.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import proiect_ps.demo.Facade.UserFacade;
import proiect_ps.demo.Model.User;

import java.util.List;

/**
 * Aceasta clasa cuprinde metodele de logica pentru tabela de users, pentru a accesa fie toate inregistrarile din tabela, fie doar un
 * in functie de campul id sau user, sau pentru a inregistra un nou user in tabela si asa mai departe.
 */

@Service
public class UserService {

    @Autowired
    private final UserFacade userFacade;

    public UserService(UserFacade userFacade)
    {
        this.userFacade=userFacade;
    }

    /**
     * Aceasta metoda verifica daca un user, care este de tip unic se afla deja inregistrat in tabela users sau nu se afla.
     * @param userStr
     * @return true daca nu este inregistrat in baza de date, si false in caz contrar
     */
    public boolean verifyUser(String userStr){
        User user = userFacade.getUserByName(userStr);
        if (user == null)
            return true;
        return false;
    }

    /**
     * @return lista tuturor utilizatorilor inregistrati in tabela users.
     */
    public List<User> getAllUsers()
    {
        return userFacade.getUsers();
    }

    /**
     * Adauga un nou utlizitaor in baza de date daca nu mai exista deja in baza de date un utilizator cu acelasi username
     * @param newUser
     * @return utilizatorul proaspat adaugat.
     */
    public User addNewUser(User newUser)
    {
        if(this.verifyUser(newUser.getUser())) {
           return userFacade.addUser(newUser);
        }
        return null;
    }

    /**
     * @param id
     * @return un utilizator in functie de id-ul dat ca parametru.
     */
    public User getOneUser(Long id)
    {
        return userFacade.getUserById(id);
    }

    /**
     * @param user
     * @return un utilizator in functie de username-ul dat ca si parametru.
     */
   public User getUser(String user)
   {
       return  userFacade.getUserByUsername(user);
   }


}
