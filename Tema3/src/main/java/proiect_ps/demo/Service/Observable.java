package proiect_ps.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import proiect_ps.demo.Facade.HolidayFacade;
import proiect_ps.demo.Facade.UserFacade;
import proiect_ps.demo.Model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Aceasta clasa este folosita pentru a notifica toti observerii, adica userii atunci cand se intampla un eveniment, in cazul nostru, cand este adaugata
 * o noua oferta de vacanta.
 */
public class Observable {

    @Autowired
    private final UserFacade userFacade;

    private List<User> observers = new ArrayList<>();

    public Observable(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    /**
     * Aceasta metoda adauga intr-un array de observerii, adica de utilizatori, toti utilizatorii existenti in tabela users.
     */
    public void addObservers()
    {
        this.observers.addAll(userFacade.getUsers());
    }


    /**
     * Aceasta metoda este folosita pentru a notifica toti utilizatorii inregistrati cu mesajul returnat de metoda update.
     */
    public void notifyUsers()
    {
        this.addObservers();
        for(User user:observers)
        {
            user.update();
        }
    }


}
