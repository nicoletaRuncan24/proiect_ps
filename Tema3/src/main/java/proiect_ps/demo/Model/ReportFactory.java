package proiect_ps.demo.Model;

import java.util.List;

/**
 * In aceasta clasa este implementat pattern-ul.
 */
public class ReportFactory {

    public ReportFactory()
    {

    }

    /**
     * In aceasta metoda, in functie de parametrul reportType, se decide ce fel de oobiect se va crea, daca un obiect de tipul
     * UserReport sau un obiect de tipul LocationsReport.
     * @param users
     * @param reservations
     * @param holidays
     * @param reportType
     * @return un obiect de tipul Report
     */
    public Report createReport(List<User> users,List<Reservations> reservations, List<Holiday> holidays, String reportType )
    {
        Report report=null;
        ReportType type=ReportType.valueOf(reportType);
        if(type!=null)
        {
            switch(type)
            {
                case USER_REPORT:
                    report=new UserReport(users, reservations,holidays);
                 break;

                case LOCATION_REPORT:
                    report=new LocationsReport(users, reservations, holidays);
                break;
                default:
                    System.err.println("Unknown/unsupported account-type.");


            }
        }
        else
        {
            System.err.println("Undefined account-type.");

        }
        return report;
    }
}
