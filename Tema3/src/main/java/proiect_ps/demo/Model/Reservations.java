package proiect_ps.demo.Model;

import javax.persistence.*;

/**
 * Aceasta clasa este folosita pentru reprezentarea unei tabele in baza de date, avand 3 atribute(3 coloane in baza de date)( un id care este unic si nu va fi
 * repetat niciodata, id-ul unui user si id-ul unei locatii). Aceasta clasa ne ajuta pentru pastrarea unei rezervari de oferta a unui user din tabela user . Adnotatiile
 * folosite ajuta la crearea tabelei de rezervari din baza de date, iar parametrii adnotatiilor seteaza constrangerile pentru fiecare coloana din tabela.
 */
@Entity
@Table(name = "reservations")
public class Reservations {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_user", nullable = false)
    private Long idUser;

    @Column(name = "id_locatie", nullable = false)
    private Long idLocatie;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdLocatie() {
        return idLocatie;
    }

    public void setIdLocatie(Long idLocatie) {
        this.idLocatie = idLocatie;
    }
}
