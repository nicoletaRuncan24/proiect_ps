# Proiect Proiectare Software
## _Travel Application_

Runcan Nicoleta, Grupa 30239
![N|Solid](https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/travel2-1585330768.jpg?resize=480:*)

## Descriere generala
Aplicatia propusa de catre mine este o aplicatie care vine in ajutorul tuturor iubitorilor de calatorii si vacante peste tot in lume. A fost gandita pentru a le oferi oamenilor posibilitatea de a gasi cele mai ofertante pachete de vacante in functie de diferite perioade. 

## Functionalitati
Aplicatia dispune de urmatoarele functionalitati:

- crearea unui cont pentru un utilizator nou
- posibilitatea de a cauta oferte de vacanta in functie de locatie
- posibilitatea de a rezerva o anumita vacanta, ceea ce inseamna eliminarea ei din lista de oferte
- posibilitatea de a vizualiza toate ofertele disponibile
- posibilitatea de a adauga oferte noi de vacanta
- posibilitatea de a actualiza diferite oferte(schimbarea pretului, a perioadei)

## Proiectare/Implementare
Pana in momentul actual au fost implementate clasele model pentru Utilizator si pentru oferte de vacanta, continute de pachetul Model. Aceste doua clase sunt clasele de baza ale aplicatiei prin intermediul carora sunt create si tabelele "users" si "holiday" din baza de date. Aceste doua clase contin atribute foarte specifice obiectelor pe care le definesc, iar ca si metode, doar getters si setters. De asemenea au mai fost implementate si doua interfete "UserRepository" si "HolidayRepository" care participa la conexiunea dintre aplicatie si baza de date. Aceste doua clase sunt continute de pachetul DAO(Data Acces Object). Alte doua clase importante sunt "UserController" si "HolidayController" prin intermediul carora se acceseaza datele din baza de date. Acest lucru este posibil prin intermediul metodelor de tip PUT, GET, POST si DELETE. Cu ajutorul acestor tipuri de metode, au fost implementate metode de accesarea a tuturor elementelor din cele doua tabele, metode prin care se insereaza elemente in aceste doua tabele, metoda prin care se actualizeaza pretul unei oferte de vacanta si perioada acesteia si asa mai departe.Aceste clase sunt continute de pachetul Controller. 
De asemenea, aceste metode create au fost testate atat cu JUnit, cat si cu Postman. S-a reusit si conectarea la baza de date, iar operatiile implementate sunt vizibile in aceasta 
     
Un nou pachet a mai fost introdus, acesta fiind pachetul Facade, care contine doua clase cate una pentru cele doua tabele din baza de date. Practic acest pachet contine o instanta ale claselor de Repository, si face legatura direct dintre aceste clase de Repository continute de pachetul DAO si clasele de logica, continute de pachetul Service

## Observer Pattern
A fost implementat pattern-ul Observer pentru a notifica toti utilizatorii existenti ai aplicatiei atunci cand o noua oferta de vacanta a fost adaugata.
In implementarea acestui pattern, am implementat in clasa User metoda update, care printeaza in consola un mesaj pentru fiecare utilizator cum ca o noua oferta de vacanta a fost adaugata. Am implementat de asemenea si o casa Observable, in care exista ca si atribut o lista de users si in care se afla doua metode. O metoda de addUsers prin care in List-ul de users adaug toti utilizatorii inregistrati in tabela users, iar in cealalta metoda,parcurg toti utilizatorii si apelez metoda de update pentru fiecare dintre ei. Aceasta metoda de notifyUsers este apelata in metoda de adaugare in baza de date a ofertelor de vacanta din clasa HolidayService.
Pentru acest Pattern a fost implementata si o clasa de test, in care testam daca pentru fiecare utilizator a fost apelata metoda de update la apelarea metodei notifyUsers().

## Factory Pattern
Acest pattern a fost utilizat in crearea unor rapoarte pentru aceasta aplicatie. Se creaza 2 tipuri de rapoarte, unul pentru a afisa toate rezervarile facute de un anumit utilizator, adica istoricul utilizatorului in acea aplicatie, iar cel de-al doilea raport contine toate ofertele de vacanta existente intr-o anumita locatie, adica cu date si pret diferit. Acest pattern ne ajuta sa rezolvam problema, in care dorim sa pastram abstractizarea, fara a specifica cu exactitate clasa obiectului de tip raport pe care dorim sa il creeam (sau decidem ca trebuie creat).In comparatie cu crearea obisnuita a unui obiect prin apelarea cosntructorului, obiectul nostru de tip raport va fi creat cu ajutorul unei metode createReport(), din clasa ReportFactory, in functie de tipul acestei raport, daca este un USER_REPORT sau LOCATION_REPORT.

## Endpoints
 - In clasa UserController:
 --folosesc trei GetMapping-uri, unul pentru a afisa toti utilizatorii existenti in baza de date, unul pentru a afisa utilizatorul care are un anumit id, iau unul pentru a afisa utilizatorul care are un anumit username.
 --folosesc un PostMapping pentru a adauga un nou utilizator in baza de date

 - In clasa HolidayController:
 --folosesc trei GetMapping-uri, unul pentru a afisa toate ofertele de vacanta existente, unul pentru a afisa o anumita oferta de vacanta in functie de id, iar unul care sa imi afiseze toate ofertele de vacanta dintr-o anumita locatie.
 --folosesc un PostMapping pentru a aduga o noua oferta de vacanta.
 --folosesc un PutMapping pentru a face un update la ofertele de vacanta la pret si perioada.
 --folosesc un DeleteMapping pentru a sterge o oferta in functie de id

- In clasa ReservationsController:
 --foloses 2 getMapping-uri, unul pentru a afisa toate rezervarile existente, iar celalalt pentru a afisa toate rezervarile existente pentru un anumit user
 --folosesc un PostMapping pentru a adauga o noua rezervare de vacanta
 



