package proiect_ps.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import proiect_ps.demo.DAO.HolidayRepository;
import proiect_ps.demo.Model.Holiday;
import proiect_ps.demo.Model.User;
import proiect_ps.demo.Service.HolidayService;
import proiect_ps.demo.Service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Clasa HolidayController participa la interactiunea cu web services prin metode tip GET, PUT, POST si DELETE pentru transmiterea datelor dintre
 * aplicatie si baza de date, dar si invers(tabela holiday).
 */
@RestController
@RequestMapping("/holiday")
public class HolidayController {

    @Autowired
    private HolidayService holidayServ;

    /**
     * Aceasta metoda participa la afisarea tuturor obiectelor de tip Holiday retinute in baza de date a aplicatiei.
     * @return lista tuturor vacantelor disponibile din aplicatie
     */
    @GetMapping("/getHoliday")
    public List<Holiday> all() {
        return holidayServ.getAllH();
    }

    /**
     * Aceasta metoda returneaza un obiect de tipul Holiday din baza de date, care are un anumit id dat ca si parametru.
     * @param id
     * @return un obiect de tip Holiday care are un anumit id
     */
    @GetMapping("/getHolidayID/{id}")
    public Holiday getOneHolidayID(@PathVariable Long id) {

        return holidayServ.getHolidayID(id);

    }

    /**
     * Aceasta metoda participa la inserarea unei noi oferte de vacanta in baza de date.
     * @param newHoliday
     * @return obiectul proaspat inserat in baza de date
     */
    @PostMapping(path="/addHoliday", consumes="application/json", produces="application/json")
    public Holiday newHoliday(@RequestBody Holiday newHoliday) {
        return holidayServ.addHoliday(newHoliday);
    }

    /**
     * Aceasta metoda participa la actualizarea campurilor de pret, data de inceput a vacantei si data de terminare a vacantei.
     * @param newHoliday
     * @param id
     * @return obiectul actualizat cu modificarile de riguoare aplicate asupra sa..
     */
    @PutMapping("/holiday/{id}")
    public Holiday replaceHoliday(@RequestBody Holiday newHoliday, @PathVariable Long id) {

        return holidayServ.replaceHoliday(newHoliday, id);
    }

    /**
     * Aceasta metoda participa la stergerea unei oferte din baza de date, in functie de id.
     * @param id
     */
    @DeleteMapping("/deleteHoliday/{id}")
    public void deleteHoliday(@PathVariable Long id) {
        holidayServ.deleteH(id);
    }

    /**
     * Aceasta metoda prin request-ul GetMapping, ne afiseaza lista tuturor ofertelor dintr-o anumita locatie.
     * @param location
     * @return
     */
    @GetMapping("/getOffers/{location}")
    public List<Holiday> getAllOffersByLocation(@PathVariable String location) {
        return holidayServ.getOffers(location);
    }


}
