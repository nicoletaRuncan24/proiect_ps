package proiect_ps.demo.Model;

/**
 * Interfata care participa la implementarea Observer-ului si are o singura metoda in ea, cea de update.
 */
public interface UserInterface {
    public void update();
}
