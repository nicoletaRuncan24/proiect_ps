package proiect_ps.demo.Model;

import javax.persistence.*;

/**
 * Aceasta clasa este folosita pentru reprezentarea unei tabele in baza de date, avand 5 atribute(5 coloane in baza de date)( un id care este unic si nu va fi
 * repetat niciodata, numele unei locatii de vacanta, un pret pentru vacanta, o data de inceput a vacantei si o data de sfarsire a vacantei. Aceasta clasa ne
 * ajuta pentru pastrarea obiectelor de tip User. Adnotatiile folosite ajute la crearea tabelei users in baza de date, iar parametrii adnotatiilor seteaza
 * constrangerile pentru fiecare coloana din tabela.
 */
@Entity
@Table(name = "users")
public class User implements UserInterface{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true, length = 45)
    private String user;

    @Column(nullable = false, length = 64)
    private String password;

    @Column(name = "first_name", nullable = false, length = 50)
    private String firstName;

    @Column(name = "last_name", nullable = false, length = 50)
    private String lastName;

    public User(String user, String password, String firstName, String lastName) {
        this.user=user;
        this.password=password;
        this.firstName=firstName;
        this.lastName=lastName;
    }

    public User()
    {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String email) {
        this.user = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Prin aceasta metoda, user-ul care este un observer este notificat cu acest mesaj.
     */
    public void update()
    {
        System.out.println(this.getFirstName()+" "+this.getLastName()+" a fost adaugata o noua oferta de vacanta!");
    }
}
