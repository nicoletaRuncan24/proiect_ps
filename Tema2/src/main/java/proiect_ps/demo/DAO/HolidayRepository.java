package proiect_ps.demo.DAO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import proiect_ps.demo.Model.Holiday;
import proiect_ps.demo.Model.User;

import java.util.List;


/**
 * Aceasta clasa participa la realizarea conexiunii aplicatiei cu baza de date, mai exact cu tabela holiday.
 */
public interface HolidayRepository extends CrudRepository<Holiday, Long>{
    @Query("SELECT h FROM Holiday h WHERE h.location = ?1")
    public List<Holiday> findByLocation(String location);
}
