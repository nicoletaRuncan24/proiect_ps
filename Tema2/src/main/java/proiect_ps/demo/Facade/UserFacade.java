package proiect_ps.demo.Facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import proiect_ps.demo.DAO.UserRepository;
import proiect_ps.demo.Model.User;

import java.util.List;

/**
 * Aceasta clasa are ca si atribut o instanta a clasei UserRepository, si contine logica de baza pe tabela users din baza de date.
 */
@Service
public class UserFacade {
    @Autowired
    private final UserRepository userRepository;

    public UserFacade(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Apeleaza metoda findByUser din UserRepository pentru a gasi in tabela users o inregistrare cu username-ul dat ca parametru.
     * @param userStr
     * @return user-ul cu un anumit username
     */
    public User getUserByName(String userStr) {
        User user = userRepository.findByUser(userStr);

        return user;
    }

    /**
     * Aceasta metoda apeleaza metoda findAll pentru a cauta in baza de date toti utilizatorii inregistrati din tabela users.
     * @return toti utilizatorii existenti
     */
    public List<User> getUsers()
    {

        return (List<User>)userRepository.findAll();
    }

    /**
     * Aceasta metoda apeleaza metoda save pentru a adauga o noua inregistrare in tabela users.
     * @param newUser
     * @return utilizatorul proaspat adaugat.
     */
    public User addUser(User newUser)
    {
        return userRepository.save(newUser);
    }

    /**
     * Aceasta metoda apeleaza metoda findById pentru a gasi user-ul din tabela users care are id-ul dat ca si parametru.
     * @param id
     * @return utilizatorul din tabela users care are id-ul id.
     */
    public User getUserById(Long id)

    {
        return userRepository.findById(id).get();
    }

    /**
     * Aceasta metoda apeleaza metoda findByUser pentru a gasi in tabela users, utilizatorul cu username-ul dat ca parametru
     * @param user
     * @return user-ul care are un username-ul dat ca si parametru.
     */
    public User getUserByUsername(String user)
    {
        return  userRepository.findByUser(user);
    }

}
