package proiect_ps.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import proiect_ps.demo.DAO.HolidayRepository;
import proiect_ps.demo.DAO.UserRepository;
import proiect_ps.demo.Facade.HolidayFacade;
import proiect_ps.demo.Facade.UserFacade;
import proiect_ps.demo.Model.Holiday;
import proiect_ps.demo.Model.User;
import proiect_ps.demo.Service.HolidayService;

import java.awt.*;
import java.time.LocalDate;
import java.util.List;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
@ExtendWith(MockitoExtension.class)
public class ObservableTest {

    @Mock
    HolidayRepository holidayRep;

    @Mock
    List<User> users;

    @Mock
    UserRepository userRep;

    @Test
    public void TestObserver()
    {
        Holiday holiday=new Holiday("Dubai", 900, LocalDate.of(2021, 8, 9), LocalDate.of(2021, 8, 19));
        HolidayFacade holidayFacade=new HolidayFacade(holidayRep);
        UserFacade userFacade=new UserFacade(userRep);
        HolidayService holidayServ = new HolidayService(holidayFacade, userFacade);
        holidayServ.notifyUsers();
        users=userFacade.getUsers();
        for(User user:users)
        {
            verify(user, atLeastOnce()).update();
        }
    }


}
