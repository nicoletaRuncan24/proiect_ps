package proiect_ps.demo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import proiect_ps.demo.DAO.UserRepository;
import proiect_ps.demo.Facade.UserFacade;
import proiect_ps.demo.Model.User;
import proiect_ps.demo.Service.UserService;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class UserRepositoryTests {

   @Mock
    UserRepository userRepository;

    @Test
    public void testUser() {
        assertNotNull(userRepository);
        User user = new User("mircea.stan", "mircea123", "Mircea", "Stan");
        UserFacade userFacade= new UserFacade(userRepository);
        UserService userServ = new UserService(userFacade);
        userServ.verifyUser(user.getUser());
        verify(userRepository, atLeastOnce()).findByUser(user.getUser());
    }

    @Test
    public void testGetUsers()
    {
        UserFacade userFacade= new UserFacade(userRepository);
        UserService userServ = new UserService(userFacade);
        userServ.getAllUsers();
        verify(userRepository, atLeastOnce()).findAll();
    }

    @Test
    public void testAddUsers()
    {
        UserFacade userFacade= new UserFacade(userRepository);
        UserService userServ = new UserService(userFacade);
        User user = new User("mircea.stan", "mircea123", "Mircea", "Stan");
        userServ.addNewUser(user);
        verify(userRepository, atLeastOnce()).save(user);
    }

    @Test void testGetOneUser()
    {
        String user="nicoleta.runcan";
        UserFacade userFacade= new UserFacade(userRepository);
        UserService userServ = new UserService(userFacade);
        userServ.getUser(user);
        verify(userRepository, atLeastOnce()).findByUser(user);
    }





}