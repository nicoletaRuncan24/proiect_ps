package proiect_ps.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;
import proiect_ps.demo.Controller.HolidayController;
import proiect_ps.demo.DAO.HolidayRepository;
import proiect_ps.demo.DAO.UserRepository;
import proiect_ps.demo.Model.Holiday;
import proiect_ps.demo.Model.User;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
@ExtendWith(MockitoExtension.class)
public class HolidayRepositoryTests {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private HolidayRepository repo;

    @Mock
    HolidayController holidayC;

    @Test
    public void testCreateHoliday() {
        Holiday holiday = new Holiday();
        holiday.setLocation("Mexic");
        holiday.setStartDate(LocalDate.of(2021, 07, 12));
        holiday.setEndDate(LocalDate.of(2021, 07, 18));
        holiday.setPrice(800);

        Holiday savedHoliday = repo.save(holiday);

        Holiday existHoliday = entityManager.find(Holiday.class, savedHoliday.getId());

        assertThat(holiday.getLocation()).isEqualTo(existHoliday.getLocation());
    }

    @Test
    public void testAddFunction()
    {
        Holiday holiday=new Holiday("Dubai", 900, LocalDate.of(2021, 8, 9), LocalDate.of(2021, 8, 19));
        HolidayController holidayController=mock(HolidayController.class);
        holidayController.newHoliday(holiday);
        verify( holidayController, atLeastOnce()).newHoliday(holiday);
    }

    @Test
    public void testReplaceHoliday()
    {
        Holiday holiday=new Holiday("Mexic", 1000, LocalDate.of(2021, 9, 20), LocalDate.of(2021, 9, 25));
        HolidayController holidayController=mock(HolidayController.class);
        holidayController.replaceHoliday(holiday, Long.valueOf(1));
        verify( holidayController, atLeastOnce()).replaceHoliday(holiday, Long.valueOf(1));
    }

    @Test
    public void testDeleteHoliday()
    {
        HolidayController holidayController=mock(HolidayController.class);
        holidayController.deleteHoliday(Long.valueOf(1));
        verify( holidayController, atLeastOnce()).deleteHoliday(Long.valueOf(1));
    }

    @Test
    public void testGetHolidayId()
    {
        HolidayController holidayController=mock(HolidayController.class);
        holidayController.getOneHolidayID(Long.valueOf(1));
        verify( holidayController, atLeastOnce()).getOneHolidayID(Long.valueOf(1));
    }







}