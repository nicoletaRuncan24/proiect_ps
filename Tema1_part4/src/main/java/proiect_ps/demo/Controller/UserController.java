package proiect_ps.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import proiect_ps.demo.DAO.UserRepository;
import proiect_ps.demo.Model.User;

import java.util.List;
import java.util.Optional;

/**
 * Aceasta clasa participa la  interactiunea cu web services prin metode tip GET, PUT, POST si DELETE pentru transmiterea datelor dintre
 * aplicatie si baza de date, dar si invers(tabela users).
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserRepository repository;


    /**
     * Aceasta metoda participa la afisarea tuturor obiectelor de tip User retinute in baza de date a aplicatiei.
     * @return lista tuturor utilizatorilor aplicatiei.
     */
    @GetMapping("/getUser")
    public List<User> all() {
        return (List<User>)repository.findAll();
    }

    /**
     *Aceasta metoda participa la introducerea in baza de date a unui utilizator proaspat autentificat.
     * @param newUser
     * @return obiectul utilizatorului proaspat autentificat in aplicatie
     */
    @PostMapping(path="/addUser", consumes="application/json", produces="application/json")
    public User newUser(@RequestBody User newUser) {
        return repository.save(newUser);
    }

    /**
     * Aceasta metoda participa la identificarea si returnarea unui obiect de tip Utilizator din baza de date cu un anumit id.
     * @param id
     * @return utilizatorul care are id-ul dat ca si parametru functiei.
     */
    @GetMapping("/getUserID/{id}")
    public User getOneUserID(@PathVariable Long id) {

        return repository.findById(id).get();

    }

    /**
     * Aceasta metoda returneaza un obiect de tip User, care are un anumit username
     * @param user
     * @return utlizatorul care are username-ul identic ca cel dat ca si parametru functiei
     */
    @GetMapping("/getUserU/{user}")
    public  User getOneUser(@PathVariable String user) {

        return  repository.findByUser(user);

    }




}
