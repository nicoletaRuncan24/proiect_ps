package proiect_ps.demo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;
import proiect_ps.demo.Controller.HolidayController;
import proiect_ps.demo.Controller.UserController;
import proiect_ps.demo.DAO.UserRepository;
import proiect_ps.demo.Model.Holiday;
import proiect_ps.demo.Model.User;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class UserRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository repo;

    @Mock
    UserRepository userRepository;

    @Test
    public void testCreateUser() {
        User user = new User();
        user.setUser("nicoleta.runcan");
        user.setPassword("nico123");
        user.setFirstName("Nicoleta");
        user.setLastName("Runcan");

        User savedUser = repo.save(user);

        User existUser = entityManager.find(User.class, savedUser.getId());

        assertThat(user.getUser()).isEqualTo(existUser.getUser());
    }

    @Test
    public void testGetUsers()
    {
        UserController userController=mock(UserController.class);
        userController.all();
        verify( userController, atLeastOnce()).all();

    }

    @Test
    public void testAddFunction()
    {
        User newUser=new User("david.petrut", "david123", "David", "Petrut");
        UserController userController=mock(UserController.class);
        userController.newUser(newUser);
        verify( userController, atLeastOnce()).newUser(newUser);
    }

    @Test
    public void testGeOnetUser()
    {
        UserController userController=mock(UserController.class);
        userController.getOneUser("david.petrut");
        verify( userController, atLeastOnce()).getOneUser("david.petrut");
    }

    @Test
    public void testGetOneUsersById()
    {
        UserController userController=mock(UserController.class);
        userController.getOneUserID(Long.valueOf(1));
        verify( userController, atLeastOnce()).getOneUserID(Long.valueOf(1));

    }


}