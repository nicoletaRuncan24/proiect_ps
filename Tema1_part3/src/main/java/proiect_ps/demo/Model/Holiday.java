package proiect_ps.demo.Model;

import javax.persistence.*;
import javax.xml.crypto.Data;
import java.time.LocalDate;
import java.util.Date;

/**
 * Aceasta clasa este folosita pentru reprezentarea unei tabele in baza de date, avand 5 atribute(5 coloane in baza de date)( un id care este unic si nu va fi
 * repetat niciodata, un user, o parola, nume si prenume. Aceasta clasa ne ajuta pentru pastrarea obiectelor de tip Holiday. Adnotatiile folosite
 * se folosesc pentru crearea tabelei in baza de date, iar parametrii adnotatiilor seteaza constrangerile pentru fiecare coloana din tabela.
 *
 */
@Entity
@Table(name = "holiday")

public class Holiday {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column( nullable= false, length = 45)
    private String location;

    @Column( nullable = false)
    private int price;

    @Column(name = "end_date", nullable = false)
    private LocalDate endDate;

    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    /**
     * @param location
     * @param price
     * @param eDate
     * @param sDate
     * Acest constructor este folosit pentru a initializa atributele obiectului de acest tip.
     */
    public Holiday(String location, int price, LocalDate eDate, LocalDate sDate)
    {
       this.location=location;
       this.price=price;
       this.endDate=eDate;
       this.startDate=sDate;
    }


    public Holiday()
    {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
