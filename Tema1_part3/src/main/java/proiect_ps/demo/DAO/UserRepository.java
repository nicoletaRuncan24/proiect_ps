package proiect_ps.demo.DAO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import proiect_ps.demo.Model.Holiday;
import proiect_ps.demo.Model.User;

/**
 * Aceasta clasa participa la realizarea conexiunii aplicatiei cu baza de date, mai exact cu tabela users.
 */
public interface UserRepository extends CrudRepository<User, Long> {
    @Query("SELECT u FROM User u WHERE u.user = ?1")
    public User findByUser(String user);
}
